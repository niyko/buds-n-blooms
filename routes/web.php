<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ListingController;
use Illuminate\Support\Facades\Route;

Route::get('/', [ListingController::class, 'listingView'])->name('home');

Route::get('/login', [AuthController::class, 'loginView'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login.method');

Route::get('/profile/{student_id}', [StudentController::class, 'profileView'])->name('profile.page');

Route::middleware('auth')->group(function () {
    Route::get('/student/create', [StudentController::class, 'createStudentView'])->name('student.create.page');
    Route::post('/student/create', [StudentController::class, 'createStudent'])->name('student.create.method');

    Route::get('/student/edit/{student_id}', [StudentController::class, 'editStudentView'])->name('student.edit.page');
    Route::post('/student/edit/{student_id}', [StudentController::class, 'editStudent'])->name('student.edit.method');
    
    Route::get('/student/delete/{student_id}', [ListingController::class, 'deleteStudent'])->name('student.delete.method');

    Route::get('/post/create/{student_id}', [PostController::class, 'createPostView'])->name('post.create.page');
    Route::post('/post/create/{student_id}', [PostController::class, 'createPost'])->name('post.create.method');

    Route::post('/post/upload/file', [PostController::class, 'uploadFile'])->name('post.upload.file.method');

    Route::get('/post/delete/{post_id}', [PostController::class, 'deletePost'])->name('post.delete.method');
});
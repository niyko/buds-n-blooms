<?php

namespace App\Http\Controllers;

use App\Models\ProfileFields;
use App\Models\Student;
use Illuminate\Http\Request;
use Image;

class StudentController extends Controller
{
    public function createStudentView(){
        $student = null;
        $class_list = Student::select('class')->get()->pluck('class');
        return view('backend.upsert', compact('class_list', 'student'));
    }

    public function createStudent(Request $request){
        $profile_image_name = $this->saveProfileImage($request->file('profile_image'));
        $student = new Student();
        $student->name = $request->name;
        $student->class = $request->class;
        $student->biography = $request->biography;
        $student->profile_image = $profile_image_name;
        $student->save();
        return redirect()->route('home');
    }

    public function editStudentView($student_id){
        $class_list = Student::select('class')->get()->pluck('class');
        $student = Student::find($student_id);
        return view('backend.upsert', compact('class_list', 'student'));
    }

    public function editStudent($student_id, Request $request){
        if($request->file('profile_image')){
            $profile_image_name = $this->saveProfileImage($request->file('profile_image'));
        }
        $student = Student::find($student_id);
        $student->name = $request->input('name');
        $student->class = $request->input('class');
        $student->biography = $request->input('biography');
        if($request->file('profile_image')) $student->profile_image = $profile_image_name;
        $student->save();
        return redirect()->route('profile.page', ['student_id' => $student_id]);
    }

    public function saveProfileImage($file){
        $profile_image_name = hash('md5', microtime()).'.png';
        $profile_image_path = public_path('uploads/'.$file->storeAs('profile-images', $profile_image_name, 'uploads'));
        $resized_image = Image::make($profile_image_path)->fit(500, 550);
        $resized_image->save($profile_image_path, 60, 'jpg');
        return $profile_image_name;
    }

    public function profileView($student_id){
        $student = Student::find($student_id);
        $posts = ProfileFields::where('profile_id', $student_id)->orderBy('id', 'DESC')->simplePaginate(10);
        return view('profile', compact('student', 'posts'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\ProfileFields;
use App\Models\Student;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    public function listingView(Request $request){
        $search_keyword = $request->input('search');
        if($search_keyword){
            $posts = ProfileFields::select('profile_id', 'title', 'description', 'students.name', 'students.class', 'students.biography')
            ->join('students', 'profile_fields.profile_id', '=', 'students.id')
            ->where('title', 'like', '%'.$search_keyword.'%')
            ->orWhere('description', 'like', '%'.$search_keyword.'%')
            ->orWhere('name', 'like', '%'.$search_keyword.'%')
            ->orWhere('class', 'like', '%'.$search_keyword.'%')
            ->orWhere('biography', 'like', '%'.$search_keyword.'%')
            ->get()
            ->pluck('profile_id');
            $search_student_ids = array_unique($posts->toArray());
            $students = Student::whereIn('id', $search_student_ids);
        }
        else{
            $students = new Student;
        }
        $students = $students->orderBy('name')
            ->simplePaginate(20)
            ->appends(request()->input());
        return view('listing', compact('students', 'search_keyword'));
    }

    public function deleteStudent($student_id){
        Student::find($student_id)->delete();
        return back()->withErrors([
            'delete' => 'Student has been successfully deleted',
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\ProfileFields;
use App\Models\Student;
use Illuminate\Http\Request;
use Image;

class PostController extends Controller
{
    public function createPostView($student_id, Request $request){
        $student = Student::find($student_id);
        return view('backend.post', compact('student'));
    }

    public function createPost($student_id, Request $request){
        $profile_field = new ProfileFields();
        $profile_field->profile_id = $student_id;
        $profile_field->title = $request->title;
        $profile_field->description = $request->description;
        $profile_field->content = json_decode($request->post);
        $profile_field->save();
        return redirect()->route('profile.page', ['student_id' => $student_id])->withErrors([
            'create' => 'Post has been successfully added',
        ]);
    }

    public function uploadFile(Request $request){
        if(!in_array($request->file('file')->extension(), array('jpg', 'png', 'mp3', 'm4a', 'wav', 'aac', 'jpeg'))){
            return response('Invalid file extension', '404');
        }
        $type = 'image';
        if(in_array($request->file('file')->extension(), array('mp3', 'm4a', 'wav', 'aac'))) $type = 'audio';
        $file_name = hash('md5', microtime()).'.'.$request->file('file')->extension();
        if($type=='image'){
            $path = $request->file('file')->storeAs('files', $file_name, 'uploads');
            $file_path = public_path('uploads/'.$path);
            $file = Image::make($file_path);
            $file->orientate();
            $file->resize(500, null, function ($constraint) {
                $constraint->upsize();
                $constraint->aspectRatio();
            });
            $file->save($file_path, 80, $request->file('file')->extension());
        }
        else $path = $request->file('file')->storeAs('files', $file_name, 'uploads');
        return response()->json([
            'file' => $path,
            'type' => $type
        ]);
    }

    public function deletePost($post_id){
        ProfileFields::find($post_id)->delete();
        return back()->withErrors([
            'delete' => 'Post has been successfully deleted',
        ]);
    }
}

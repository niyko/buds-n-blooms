<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    
    protected $table = 'students';

    public function posts()
    {
        return $this->hasMany(ProfileFields::class, 'profile_id', 'id');
    }
}

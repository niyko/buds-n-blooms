<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileFields extends Model
{
    use HasFactory;

    protected $table = 'profile_fields';

    protected $casts = [
        'content' => 'array',
    ];
}

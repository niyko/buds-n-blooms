@extends('app')

@section('title', 'Login')

@section('body')
<div class="auth-container align-middle">
    <div class="container">
        <div class="card p-3">
            <form action="{{ route('login.method') }}" method="POST">
                @csrf
                <p class="subtitle text-center mb-2">Login</p>
                <h1 class="text-center fw-bold mb-3">{{ env('APP_NAME') }}</h1>
                @include('component.alert', ['errors' => $errors])
                <div class="mb-3">
                    <label class="form-label">Your email</label>
                    <input name="email" type="email" class="form-control" placeholder="hello@example.com" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Your password</label>
                    <input name="password" type="password" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
@endpush
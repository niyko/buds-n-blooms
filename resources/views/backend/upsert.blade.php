@extends('app')

@section('title', (isset($student)?'Edit':'Add').' student')

@push('head')
    <link rel="stylesheet" href="{{ asset('css/autocomplete.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upsert.css') }}">
@endpush

@section('body')
<div>
    <div class="banner-container">
        <p class="subtitle text-center mb-2">{{ isset($student)?'Modify':'Create' }}</p>
        <h1 class="text-center text-white fw-bold mb-3">{{ isset($student)?'Update':'Add' }} student</h1>
        <a href="{{ route('home') }}"><button class="back-btn"><i class="fas fa-arrow-left"></i> Back</button></a>
    </div>
    <div class="container">
        <form action="{{ isset($student)?route('student.edit.method', ['student_id' => $student->id]):route('student.create.method') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card mt-3">
                <div class="card-header"><i class="fas fa-user-edit"></i> Student details</div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label required">Student name</label>
                        <input name="name" type="text" class="form-control" required autoComplete="off" value="{{ isset($student)?$student->name:'' }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label required">Student class</label>
                        <input id="autoComplete" name="class" type="text" class="form-control w-100" required autoComplete="off" value="{{ isset($student)?$student->class:'' }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label required">Biography</label>
                        <textarea name="biography" class="form-control" required autoComplete="off">{{ isset($student)?$student->biography:'' }}</textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label required">Profile image</label>
                        <div class="row g-1">
                            <div class="col">
                                <input name="profile_image" type="file" class="form-control" {{ isset($student)?'':'required' }} autoComplete="off">
                            </div>
                            @if($student)
                                <div class="col-auto">
                                    <img class="form-profile-preview" src="{{ asset('uploads/profile-images/'.$student->profile_image) }}">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3">{{ isset($student)?'Modify':'Create' }}</button>
        </form>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/@tarekraafat/autocomplete.js@10.2.6/dist/autoComplete.min.js"></script>
    <script>
        const autoCompleteJS = new autoComplete({
            data: {
                src: @json($class_list),
                cache: true,
            },
            resultItem: {
                highlight: true
            },
            events: {
                input: {
                    selection: (event) => {
                        const selection = event.detail.selection.value;
                        autoCompleteJS.input.value = selection;
                    }
                }
            }
        });
    </script>
@endpush
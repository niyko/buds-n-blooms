@extends('app')

@section('title', 'student')

@push('head')
    <link rel="stylesheet" href="{{ asset('css/post.css') }}">
@endpush

@section('body')
<div>
    <div class="banner-container">
        <p class="subtitle text-center mb-2">Create</p>
        <h1 class="text-center text-white fw-bold">New post for</h1>
        <p class="post-for-text text-center mb-2 mt-0">{{ $student->name }} <span class="badge rounded-pill">{{ $student->class }}</span></p>
        <a href="{{ route('profile.page', ['student_id' => $student->id]) }}"><button class="back-btn"><i class="fas fa-arrow-left"></i> Back</button></a>
    </div>
    <div class="container">
        <form action="{{ route('post.create.method', ['student_id' => $student->id]) }}" method="POST" enctype="multipart/form-data" onsubmit="return parseMedia()">
            @csrf
            <div class="card mt-3">
                <div class="card-header"><i class="fas fa-user-edit"></i> Post details</div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label required">Post title</label>
                        <input name="title" type="text" class="form-control" required autoComplete="off" value="">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Post description</label>
                        <textarea name="description" class="form-control" height="500" autoComplete="off"></textarea>
                    </div>
                    <input name="post" type="hidden" value="{}">
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header"><i class="fas fa-photo-video"></i> Other media</div>
                <div class="card-body">
                    <div class="uploaded-media-list">
                        <div class="empty-listing-container mt-3 mb-4">
                            <img src="{{ asset('images/null.svg') }}">
                            <h3 class="mt-3 mb-0">No media added</h3>
                            <p>You can add youtube videos</p>
                        </div>
                    </div>
                    <div class="row g-2 mb-2 mt-3">
                        <div class="col-6">
                            <button class="btn btn-small btn-secondary" onclick="postYoutubeModal.show();" type="button">Add youtube</button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-small btn-secondary" onclick="postFileModal.show();" type="button">Add file</button>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-4 mb-4">Create post</button>
        </form>
    </div>
</div>

<div class="modal fade" id="post-youtube-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Youtube</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label class="form-label required">Youtube link</label>
                    <input id="youtube-link" type="text" class="form-control" required autoComplete="off" placeholder="https://youtu.be/ZCiDEc-QE5s">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-modal btn-primary" onclick="addYoutubeVideo()">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="post-file-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add File</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label class="form-label required">Upload file</label>
                    <input id="file" onchange="uploadFile()" type="file" class="form-control" required autoComplete="off" accept="image/*, audio/*">
                </div>
                <div class="progress" style="display: none;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-modal btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<template id="media-item-template" hidden>
    <div class="uploaded-media-item mb-2" data-media-url="@{{link}}" data-media-title="@{{title}}" data-media-type="@{{type}}">
        <div class="row g-0">
            <div class="col-auto">
                <button type="button"><i class="@{{icon}}"></i></button>
            </div>
            <div class="col">
                <p>@{{title}}</p>
            </div>
            <div class="col-auto">
                <button class="close-btn" onclick="removeMedia(this)" type="button"><i class="fas fa-times"></i></button>
            </div>
        </div>
    </div>
</template>
@endsection

@push('script')
<script>
    let isMediaEmpty = true;
    let mediaItemTemplate = Handlebars.compile($("#media-item-template").html());
    let postYoutubeModal = new bootstrap.Modal($("#post-youtube-modal"), { keyboard: false });
    let postFileModal = new bootstrap.Modal($("#post-file-modal"), { keyboard: false });

    function addYoutubeVideo(){
        let youtubeUrl = $("#youtube-link").val();
        if(matchYoutubeUrl(youtubeUrl)){
            if(isMediaEmpty) $(".empty-listing-container").hide();
            isMediaEmpty = false;
            $(".uploaded-media-list").append(mediaItemTemplate({
                title: youtubeUrl,
                link: getYoutubeUrlId(youtubeUrl),
                icon: "fab fa-youtube",
                type: "youtube"
            }));
            postYoutubeModal.hide();
            $("#youtube-link").val("");
        }
        else alert("Invalid youtube link");
    }

    function removeMedia(elem){
        let title = $(elem).closest(".uploaded-media-item").data("media-title");
        if(confirm(`Are you sure you want to delete ${title}`)){
            $(elem).closest(".uploaded-media-item").remove();
            if($(".uploaded-media-item").length==0) $(".empty-listing-container").show();
        }
    }

    function parseMedia(){
        let mediaData = [];
        $(".uploaded-media-item").each(function(index) {
            mediaData.push({
                url: $(this).data("media-url"),
                type: $(this).data("media-type")
            });
        });
        $("[name=post]").val(JSON.stringify(mediaData));
        return true;
    }

    function uploadFile(){
        if(isMediaEmpty) $(".empty-listing-container").hide();
        isMediaEmpty = false;
        let formData = new FormData();
        formData.append("file", $("#file")[0].files[0]);
        $(".progress").show();
        $.ajax({
            type: "POST",
            url: "{{ route('post.upload.file.method') }}",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $(".uploaded-media-list").append(mediaItemTemplate({
                    title: data.file,
                    link: data.file,
                    icon: data.type=="audio"?"fas fa-headphones-alt":"fas fa-image",
                    type: data.type
                }));
                postFileModal.hide();
                $("#file").val("");
                $(".progress").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".progress").hide();
                alert("Something went wrong");
            }
        })
    }

    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if(url.match(p)){
            return url.match(p)[1];
        }
        return false;
    }

    function getYoutubeUrlId(url) {
        const urlObject = new URL(url);
        let urlOrigin = urlObject.origin;
        let urlPath = urlObject.pathname;
        if (urlOrigin.search('youtu.be') > -1) {
            return urlPath.substr(1);
        }
        if (urlPath.search('embed') > -1) {
            return urlPath.substr(7);
        }
        return urlObject.searchParams.get('v');
    }
</script>
@endpush
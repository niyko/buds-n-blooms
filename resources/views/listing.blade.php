@extends('app')

@section('body')
<div>
    <div class="banner-container listing-banner-container" style="background-color: {{ env('APP_HEADER_COLOR') }};">
        <div class="w-100 text-center">
            <img class="listing-logo" src="{{ asset('images/logo.png') }}">
        </div>
        <form action="" method="GET">
            <div class="search-box mt-3">
                <div class="row g-0">
                    @if($search_keyword!='')
                        <div class="col-auto">
                            <a class="clear-btn" href="?search=">
                                <i class="fas fa-times"></i>
                            </a>
                        </div>
                    @endif
                    <div class="col">
                        <input name="search" type="text" placeholder="Search anything" value="{{ $search_keyword }}">
                    </div>
                    <div class="col-auto">
                        <button type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container mt-3">
        @include('component.alert', ['errors' => $errors])
        <div class="row">
            @forelse ($students as $student)
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card listing-card mb-3">
                        <div class="row g-0">
                            <div class="col-4">
                                <img src="{{ asset('uploads/profile-images/'.$student->profile_image) }}" class="profile-image img-fluid rounded-start object-fit-cover h-100">
                            </div>
                            <div class="col-8">
                                <div class="card-body">
                                    <a href="{{ route('profile.page', ['student_id' => $student->id]) }}">
                                        <h5 class="card-title fw-bold">{{ $student->name }}</h5>
                                        <p class="card-text profile-biography">{{ $student->biography }}</p>
                                        <p class="card-text mb-0"><small class="text-muted">Studing in <span class="badge rounded-pill">{{ $student->class }}</span></small></p>
                                    </a>
                                    @auth
                                        <a class="listing-card-delete-btn" href="{{ route('student.delete.method', ['student_id'=>$student->id]) }}" onclick="return confirm('Are you sure you want to delete?')"><i class="fas fa-trash"></i></a>
                                    @endauth
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="empty-listing-container col-12 mt-5">
                    <img class="mt-5" src="{{ asset('images/null.svg') }}">
                    <h3 class="mt-3 mb-0">No students found</h3>
                    <p>Please add students or close search</p>
                </div>
            @endforelse
            <div class="row pagination mt-3 text-center">
                {{ $students->links() }}
            </div>
        </div>
        @auth
            <a class="floating-add-btn" href="{{ route('student.create.page') }}">+</a>
        @endauth
    </div>
</div>
@endsection

@push('script')
@endpush
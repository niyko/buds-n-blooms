@if($errors->any())
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
            <div><i class="fas fa-exclamation-triangle"></i> {{ $error }}</div>
        @endforeach
    </div>
@endif
@extends('app')

@section('title', $student->name)

@push('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery@2.1.8/css/lightgallery-bundle.min.css">
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
@endpush

@section('body')
<div>
    <div class="profile-banner-container" style="background-image: url('{{ asset('uploads/profile-images/'.$student->profile_image) }}');">
        <div class="profile-banner-inner">
            <img class="profile-circle-img" src="{{ asset('uploads/profile-images/'.$student->profile_image) }}">
            <h1 class="text-center text-white fw-bold mt-3 mb-0">{{ $student->name }}</h1>
            <p class="profile-class-text text-center mb-2 mt-0">Studing in <span class="badge rounded-pill">{{ $student->class }}</span></p>
            <a href="{{ route('home') }}"><button class="back-btn"><i class="fas fa-arrow-left"></i> Back</button></a>
            @auth
                <a class="profile-edit-link" href="{{ route('student.edit.page', ['student_id'=>$student->id]) }}"><i class="fas fa-pen"></i> Edit profile</a>
            @endauth
        </div>
    </div>
    <div class="container mb-3 mt-3" id="gallery">
        @include('component.alert', ['errors' => $errors])
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-header"><i class="fas fa-user-circle"></i> Biography</div>
                    <div class="card-body">
                        <p>{{ $student->biography }}</p>
                    </div>
                </div>
            </div>
            @foreach($posts as $post)
                <div class="col-12 col-md-4">
                    <div class="card mt-3">
                        <div class="card-header"><i class="fas fa-quote-left"></i>  {{ \Carbon\Carbon::parse($post->created_at)->diffForhumans() }}</div>
                        <div class="card-body">
                            @auth
                                <a class="listing-card-delete-btn" href="{{ route('post.delete.method', ['post_id'=>$post->id]) }}" onclick="return confirm('Are you sure you want to delete?')"><i class="fas fa-trash"></i></a>
                            @endauth
                            <h5 class="fw-bold mb-0">{{ $post->title }}</h5>
                            <p class="mt-0">{{ $post->description }}</p>
                            <div class="row g-1">
                                @foreach($post->content as $media)
                                    <div class="{{ (count($post->content)>1)?'col-6':'col-12' }}">
                                        @if($media['type']=='youtube')
                                            <div class="media post-card post-youtube-card {{ (count($post->content)==1)?'post-card-full':'' }}" data-src="https://www.youtube.com/watch?v={{$media['url']}}" style="background-image: url('http://img.youtube.com/vi/{{$media['url']}}/hqdefault.jpg')">
                                                <button type="button"><i class="fas fa-play"></i></button>
                                            </div>
                                        @elseif($media['type']=='audio')
                                            <div class="media post-audio-card post-card {{ (count($post->content)==1)?'post-card-full':'' }}" data-video='{"source": [{"src":"{{ asset('uploads/'.$media['url']) }}", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}'>
                                                <button type="button"><i class="fas fa-play"></i></button>
                                            </div>
                                        @elseif($media['type']=='image')
                                            <div class="media post-image-card post-card {{ (count($post->content)==1)?'post-card-full':'' }}" data-src="{{ asset('uploads/'.$media['url']) }}" style="background-image: url('{{ asset('uploads/'.$media['url']) }}')">
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row pagination mt-4 pb-3 text-center">
            {{ $posts->links() }}
        </div>

        @auth
            <a class="floating-add-btn" href="{{ route('post.create.page', ['student_id' => $student->id]) }}">+</a>
        @endauth
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@2.1.8/lightgallery.umd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@2.1.8/plugins/video/lg-video.umd.js"></script>
    <script>
        lightGallery(document.getElementById("gallery"), {
            plugins: [lgVideo],
            speed: 500,
            download: false,
            selector: ".media",
            mobileSettings: { controls: false, showCloseIcon: true, download: false } 
        });
    </script>
@endpush
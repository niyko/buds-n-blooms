<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileFieldsTable extends Migration
{
    public function up()
    {
        Schema::create('profile_fields', function (Blueprint $table) {
            $table->id();
            $table->integer('profile_id');
            $table->string('type', 100)->comment('Values can be TEXT, IMAGE, VIDEO, AUDIO');
            $table->string('title', 100);
            $table->string('description', 300);
            $table->longText('content', 100)->comment('Can be full text or file URL');;
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('profile_fields');
    }
}
